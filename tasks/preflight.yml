---

- name: Check if CiviCRM is installed
  stat:
    path: "{{ civicrm_wordpress_plugins_dir }}"
  register: civicrm_is_installed

- name: Creating a backup of the current CiviCRM installation
  block:
    - name: Create backup location for CiviCRM's SQL
      file:
        path: "{{ civicrm_backup_location }}/sql/"
        state: directory

    - name: Create backup location for CiviCRM's files
      file:
        path: "{{ civicrm_backup_location }}/files/"
        state: directory

    - name: Create a backup of CiviCrm's database
      mysql_db:
        state: dump
        name: "{{ civicrm_database_name }}"
        target: "{{ civicrm_backup_location }}/sql/{{ civicrm_database_name }}.sql"


    - name: Create a backup of Civicrm's files
      copy:
        dest: "{{ civicrm_backup_location }}/files/" # required. Remote absolute path where the file should be copied to. If C(src) is a directory, this must be a directory too. If C(dest) is a non-existent path and if either C(dest) ends with "/" or C(src) is a directory, C(dest) is created. If I(dest) is a relative path, the starting directory is determined by the remote host. If C(src) and C(dest) are files, the parent directory of C(dest) is not created and the task fails if it does not already exist.
        src:  "{{ civicrm_wordpress_plugins_dir }}" # not required. Local path to a file to copy to the remote server. This can be absolute or relative. If path is a directory, it is copied recursively. In this case, if path ends with "/", only inside contents of that directory are copied to destination. Otherwise, if it does not end with "/", the directory itself with all contents is copied. This behavior is similar to the C(rsync) command line tool.
        backup: True # not required. Create a backup file including the timestamp information so you can get the original file back if you somehow clobbered it incorrectly.
        directory_mode: True   # not required. When doing a recursive copy set the mode for the directories. If this is not set we will use the system defaults. The mode is only set on directories which are newly created, and will not affect those that already existed.
        remote_src: True # not required. Influence whether C(src) needs to be transferred or already is present remotely. If C(no), it will search for C(src) at originating/master machine. If C(yes) it will go to the remote/target machine for the C(src). C(remote_src) supports recursive copying as of version 2.8. C(remote_src) only works with C(mode=preserve) as of version 2.6.

  when: civicrm_is_installed.stat.exists
